# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Adminer
      module Build
        def self.download_adminer
          path = '/var/www/html'
          version = Shell.github_latest_version('vrana/adminer')
          name = "adminer-#{version[1 .. -1]}"
          source = "https://github.com/vrana/adminer/releases/download/#{version}"
          target = "/tmp/#{name}"
          file = "#{target}.tar.gz"

          Shell.make_folders(path)
          Shell.download_file("#{source}/#{name}.php", "#{path}/adminer.php")
          Shell.download_file("#{source}/#{name}.tar.gz", file)
          Shell.inflate_file(file, '/tmp')
          Shell.move_paths("#{target}/*", path)
          Shell.remove_paths(target, file)
        end

        def self.main
          System.invoke('Download Adminer', self.method(:download_adminer))
        end
      end

      module Run
        def self.main
          path = '/var/www/html'
          Shell.update_user
          Shell.change_owner(path)
          System.execute('php', { S: '[::]:80', t: path }, '')
        end
      end
    end
  end
end
