<?php

if ('cli' !== PHP_SAPI || 2 !== $_SERVER['argc']) {
  exit;
}

$name = $_SERVER['argv'][1];

// Sanity checks.
if (basename($name) !== $name) {
  fwrite(STDERR, "Refusing to load plugin file '${name}' for security reasons.\n");
  exit(1);
}

$file = "plugins/${name}.php";

if (!is_readable($file)) {
  fwrite(STDERR, "Unable to find plugin file '${name}' .\n");
  exit(1);
}

// Try to find class.
$code = file_get_contents($file);
$tokens = token_get_all($code);

$classFound = false;
$classes = [];

for ($index = 0, $max = count($tokens); $index < $max; $index++) {
  if (T_CLASS === $tokens[$index][0]) {
    $classFound = true;
  }

  if ($classFound && T_STRING === $tokens[$index][0]) {
    $classes[] = $tokens[$index][1];
    $classFound = false;
  }
}

// Sanity checks.
if (0 === count($classes)) {
  fwrite(STDERR, "Unable to load plugin file '${name}', because it does not define any classes.\n");
  exit(1);
}

if (1 > count($classes)) {
  fwrite(STDERR, "Unable to load plugin file '${name}', because it defines multiple classes.\n");
  exit(1);
}

// Check constructor.
$class = $classes[0];
require($file);

$require = var_export($file, true);
$constructor = (new \ReflectionClass($class))->getConstructor();

if ($constructor && $constructor->getNumberOfRequiredParameters() > 0) {
  $plugin = getcwd() . "/plugins-enabled/${name}.php";
  $parameters = $constructor->getParameters();

  $requiredParameters = implode(', ', array_map(function ($item) {
    return $item->getName();
  }, array_slice($parameters, 0, $constructor->getNumberOfRequiredParameters())));

  $constructorParameters = implode(",\n\t", array_map(/**
   * @throws ReflectionException
   */ function ($item) {
    $name = $item->getName();
    $value = $item->isOptional() ? var_export($item->getDefaultValue(), true) : '???';
    return "\$${name} = ${value}";
  }, $parameters));

  $errors = [
    "Unable to load plugin file '${name}', because it has required parameters: ${requiredParameters} ",
    "Create a file '${plugin}' with the following contents to load the plugin: ",
    '<?php',
    "require_once(${require});",
    $constructor->getDocComment(),
    "return new ${class}(",
    "\t${constructorParameters}",
    ');'
  ];

  fwrite(STDERR, implode("\n", $errors));
  exit(1);
}

$commands = [
  '<?php',
  "require_once(${require});",
  "return new ${class}();"
];

echo implode("\n", $commands);
exit(0);
